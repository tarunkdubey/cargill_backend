import configparser


class Config(object):
    # general
    config = configparser.ConfigParser()
    config.read(['.flaskenv',])
    SECRET_KEY = config['conf']['SECRET_KEY']

    # Database
    SQLALCHEMY_DATABASE_URI = "mysql://"+config['conf']['MYSQL_USER']+":"+config['conf']['MYSQL_PASSWORD']\
                              + "@" + config['conf']['MYSQL_HOST']+"/" + config['conf']['MYSQL_DB']
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

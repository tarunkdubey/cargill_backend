from . import db
from datetime import datetime

class TeamRoles(db.Model):
    """Data model for team_roles"""

    __tablename__ = 'team_roles'
    id = db.Column(
        db.Integer,
        primary_key=True
    )
    team_name = db.Column(
        db.String(64),
        index=False,
        unique=False,
        nullable=False
    )
    role_name = db.Column(
        db.String(64),
        index=False,
        unique=False,
        nullable=False
    )

    created = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False
    )
    __table_args__ = (db.UniqueConstraint('team_name', 'role_name', name='_team_role_uc'),)

    @staticmethod
    def insert_team_role(team_name, role_name):
        team_role = TeamRoles(
            team_name=team_name,
            role_name=role_name,
            created=datetime.now()
        )
        db.session.add(team_role)
        db.session.commit()
        return True

    @staticmethod
    def get_team_roles(team_name):
        team_roles = db.session.query(TeamRoles).filter(TeamRoles.team_name == team_name).all()
        response_list = []
        for team_role in team_roles:
            response_list.append({
                "team_name": team_role.team_name,
                "role_name": team_role.role_name
            })
        return response_list

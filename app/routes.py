from app import app
from app.views import insert_team_role, get_team_role

from flask import  render_template

@app.route('/api/docs')
def get_docs():
    print('sending docs')
    return render_template('swaggerui.html')

app.add_url_rule('/api/createTeamRole', methods=["POST"], view_func=insert_team_role)
app.add_url_rule('/api/getTeamRole', view_func=get_team_role)
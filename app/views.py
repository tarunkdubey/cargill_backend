from flask import request
from flask import jsonify
from .models import TeamRoles
from sqlalchemy import exc

from flask_cors import cross_origin
@cross_origin()
def insert_team_role():
    team_name = request.json.get('team_name')
    role_name = request.json.get('role_name')
    if not team_name or not role_name:
        raise ValueError(" team_name and role_name required")
    try:
        TeamRoles.insert_team_role(team_name, role_name)
    except exc.IntegrityError:
        return {"error":"team_name and role_name already exists"}, 400

    return jsonify({"success": True})

@cross_origin()
def get_team_role():
    team_name = request.args.get('team_name')
    if not team_name:
        raise ValueError("team_name required")

    roles = TeamRoles.get_team_roles(team_name)
    return jsonify(roles)
